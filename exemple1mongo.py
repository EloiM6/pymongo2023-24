from pymongo import MongoClient
import pymongo
from datetime import datetime
from pprint import pprint

uri = ""

client = MongoClient(uri)
pprint(client.list_database_names())

db = client['sample_airbnb']
print("La BD es:")
pprint(db)
coleccion = db['listingsAndReviews']
print("Longitud: " + str(coleccion.count_documents({})))

print("La coleccio es: ")
pprint(coleccion)

doc = coleccion.find_one()
print("Un primer documento: ")
pprint(doc)
print("Nom del lloguer: ")
pprint(doc['name'])

print("Exemple array. La primera comoditat: ")
pprint(doc['amenities'][0])

print("Exemple objecte. URL de la imatge: ")
pprint(doc['images']['picture_url'])

minimum_nights = coleccion.find({'minimum_nights': '8'})
print("Exemple de find: ")


#Comentat perquè escriu moltes linies
'''
for i in minimum_nights:
    pprint(i)
else:
    print("Final bucle")
'''
minimum_nights = coleccion.find({"minimum_nights": '8'}, {"_id":0, "name":1}).sort("name", pymongo.ASCENDING)

print("Exemple de find amb projecció i ordenació: ")
for i in minimum_nights:
    pprint(i)
else:
    print("Final bucle")
'''
print("Exemple Insert:")
lloc = {"name": "Exemple", "last_scraped":datetime.now(), "minimum_nights": "8", "ameneties" : ["TV", "wifi","Shampoo"],"price":10.5}
coleccion.insert_one(lloc)
minimum_nights = coleccion.find({"minimum_nights": '8'}, {"_id":0, "name":1}).sort("name", pymongo.ASCENDING)

for i in minimum_nights:
    pprint(i)
else:
    print("Final bucle")

coleccion.update_many({'name':"Exemple"},{'$set':{'beds':1}})



minimum_nights = coleccion.find({"minimum_nights": '8'}, {"_id":0, "name":1, "beds":1}).sort("name", pymongo.ASCENDING)
print("Exemple Update:")
for i in minimum_nights:
    pprint(i['name'] + ", beds: " + str(i['beds']))
else:
    print("Final bucle")



print("Exemple delete:")
coleccion.delete_many({'name':"Exemple"})

tots = coleccion.find({"minimum_nights": '8'},{"_id":0, "name":1, "beds":1}).sort("name",pymongo.ASCENDING)

for i in tots:
    pprint(i['name'] + " " + str(i['beds']))
else:
    print("Final bucle")
'''

while (True):
    lineaentrada = input("Introdueix la comanda: ").split()
    print(lineaentrada[0].lower())
    if lineaentrada[0].lower() == "fi":
        break
    elif lineaentrada[0].lower() == "google":

        entrada = lineaentrada[1]
        docGoogle = coleccion.find_one({"_id": entrada}, {"_id": 0, "name": 1, "beds": 1})
        pprint(docGoogle)
        tuplaGoogle = (docGoogle["name"], docGoogle["beds"])
        pprint(tuplaGoogle)
        docGoogle = coleccion.find_one({"_id": entrada})
        # El lloc que no té cleaning_fee és: Double Room en-suite (307) (id:10082307), Deluxe Loft Suite (id:10066928)


        if 'cleaning_fee' in docGoogle:
            tuplaGoogle2 = (docGoogle["name"], docGoogle["cleaning_fee"])
            pprint(tuplaGoogle2)
        else:
            print("No té aquest camp")
        '''
        try:
            tuplaGoogle2 = (docGoogle["name"], docGoogle["cleaning_fee"])
            pprint(tuplaGoogle2)
        except:
            print("No té aquest camp")
        '''

    else:
        print("T'has equivocat")
print("Final de programa")