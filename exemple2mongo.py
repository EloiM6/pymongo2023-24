import random

from mongoengine import *
from pprint import pprint

uri = ""

AMENITIES = ("Private pool","BBQ grill","Wheelchair accessible","Window guards","Stair gates","Standing valet","Shampoo","Dog(s)","Self check-in","Sonos sound system","Pets live on this property","Hot tub","Smoke detector","Paid parking off premises","Hot water","Hangers","Wide entryway","Bed linens","Smart lock","Bedroom comforts","Bathtub with bath chair","Private living room","Cat(s)","Hair dryer","EV charger","Baby monitor","Microwave","Fax machine","Bicycle","Private hot tub","Shared pool","Wide hallway clearance","Dining area","Pets allowed","TV","Coffee maker","Snorkeling equipment","Table corner guards","Gas oven","Private bathroom","Day bed","Beachfront","Ground floor access","Swimming pool","Chef's kitchen","Pack ’n Play/travel crib","Murphy bed","Toilet paper","Refrigerator","Elevator","Children’s dinnerware","Memory foam mattress","Cooking basics","Essentials","Lake access","DVD player","Heating","Lock on bedroom door","Host greets you","Beach essentials","Well-lit path to entrance","Breakfast table","Firm mattress","Central air conditioning","En suite bathroom","Beach view","Safety card","Buzzer/wireless intercom","Pool with pool hoist","Smart TV","Disabled parking spot","Air purifier","Kitchen","Step-free access","Internet","Stove","Family/kid friendly","Bathtub","Fireplace guards","Balcony","Convection oven","Building staff","Bath towel","Fixed grab bars for shower","Safe","Full kitchen","Room-darkening shades","Toaster","Paid parking on premises","Handheld shower head","Tennis court","24-hour check-in","Crib","Boogie boards","Patio or balcony","Breakfast bar","Parking","Long term stays allowed","Private entrance","Other pet(s)","Indoor fireplace","Alfresco shower","Wifi","Pocket wifi","translation missing: en.hosting_amenity_49","Wide doorway","Walk-in shower","Pillow-top mattress","First aid kit","Doorman","Wide clearance to shower","Fixed grab bars for toilet","Dishwasher","Ceiling fan","Heated towel rack","Dishes and silverware","Wide clearance to bed","Rain shower","Suitable for events","Hot water kettle","Permit parking","Cable TV","Washer","High chair","Double oven","Mini fridge","Ski-in/Ski-out","Lockbox","Washer / Dryer","Bidet","Gym","toilet","Sauna","Luggage dropoff allowed","Sound system","Outdoor seating","Ironing Board","Oven","Garden or backyard","Ethernet connection","Dryer","Other","Outdoor parking","Iron","Flat path to front door","Pool","Free street parking","Keypad","Bathroom essentials","Roll-in shower","Warming drawer","Kayak","Single level home","Outlet covers","Ice Machine","Netflix","Home theater","Terrace","Babysitter recommendations","Waterfront","Baby bath","Mountain view","Free parking on premises","Breakfast","Espresso machine","Extra pillows and blankets","Game console","Accessible-height bed","Children’s books and toys","Kitchenette","Electric profiling bed","Changing table","Air conditioning","translation missing: en.hosting_amenity_50","Body soap","Accessible-height toilet","Beach chairs","Laptop friendly workspace","Shower chair","Smoking allowed","Carbon monoxide detector","Fire extinguisher","Sun loungers","Formal dining area","Cleaning before checkout")
PROPERTY_TYPE=("Loft","Boutique hotel","Cottage","Treehouse","Castle","Tiny house","Train","Serviced apartment","Hut","Guest suite","House","Boat","Guesthouse","Farm stay","Townhouse","Pension (South Korea)","Condominium","Chalet","Aparthotel","Hotel","Cabin","Hostel","Casa particular (Cuba)","Villa","Apartment","Other","Resort","Heritage hotel (India)","Nature lodge","Bed and breakfast","Earth house","Barn","Bungalow","Camper/RV","Houseboat","Campsite")
ROOM_TYPE=("Entire home/apt","Shared room","Private room")
BED_TYPE=("Couch","Real Bed","Pull-out Sofa","Airbed","Futon")
SEASON=('High','Mid','Low')

client=connect(host=uri, db='sample_airbnb')
print(client)

class Imatges(EmbeddedDocument):
    thumbnail_url = StringField()
    medium_url = StringField()
    picture_url = StringField()
    xl_picture_url = StringField()
class Host(EmbeddedDocument):
    host_id = StringField()
    host_url = StringField()
    host_name = StringField()
    host_location = StringField()
    host_about = StringField()
    host_response_time = StringField()
    host_thumbnail_url = StringField()
    host_picture_url = StringField()
    host_neighbourhood = StringField()
    host_response_rate = IntField()
    host_is_superhost = BooleanField()
    host_has_profile_pic = BooleanField()
    host_identity_verified = BooleanField()
    host_listings_count = IntField()
    host_total_listings_count = IntField()
    host_verifications = ListField(StringField())

class Location(EmbeddedDocument):
    type = StringField()
    coordinates = ListField(DecimalField())
    is_location_exact = BooleanField()

class Address(EmbeddedDocument):
    street = StringField()
    suburb = StringField()
    government_area = StringField()
    market = StringField()
    country = StringField()
    country_code = StringField()
    location = EmbeddedDocumentField(Location)

class Availability(EmbeddedDocument):
    availability_30 = IntField()
    availability_60 = IntField()
    availability_90 = IntField()
    availability_365 = IntField()

class Review_Scores(EmbeddedDocument):
    review_scores_accuracy = IntField()
    review_scores_cleanliness = IntField()
    review_scores_checkin = IntField()
    review_scores_communication = IntField()
    review_scores_location = IntField()
    review_scores_value = IntField()
    review_scores_rating = IntField()

class Review(EmbeddedDocument):
    _id = LongField()
    date = DateTimeField()
    listing_id = StringField()
    reviewer_id = StringField()
    reviewer_name = StringField()
    comments  = StringField()

class ListingsAndReviews(Document):
    listing_url = StringField(required = True)
    name = StringField(required = True)
    summary = StringField()
    space = StringField()
    description = StringField()
    neighborhood_overview = StringField()
    notes = StringField()
    transit = StringField()
    access = StringField()
    interaction = StringField()
    house_rules = StringField()
    property_type = StringField(choices=PROPERTY_TYPE)
    room_type = StringField(choices=ROOM_TYPE)
    bed_type = StringField(choices=BED_TYPE)
    minimum_nights = StringField()
    maximum_nights = StringField()
    cancellation_policy = StringField()
    last_scraped = DateTimeField()
    calendar_last_scraped = DateTimeField()
    first_review = DateTimeField()
    last_review = DateTimeField()
    accommodates = IntField()
    bedrooms = IntField()
    beds = IntField()
    number_of_reviews = IntField()
    reviews_per_month = IntField()
    bathrooms = DecimalField()
    amenities = StringField(choices=AMENITIES)
    price = DecimalField()
    weekly_price = DecimalField()
    monthly_price = DecimalField()
    cleaning_fee = DecimalField()
    security_deposit = DecimalField()
    extra_people = DecimalField()
    guests_included = IntField()
    images = EmbeddedDocumentField(Imatges)
    host = EmbeddedDocumentField(Host)
    address = EmbeddedDocumentField(Address)
    availability = EmbeddedDocumentField(Availability)
    review_scores = EmbeddedDocumentField(Review_Scores)
    reviews = EmbeddedDocumentListField(Review)
    meta = {'collection': 'listingsAndReviews'}

class Customer(EmbeddedDocument):
    email=StringField()
    price_to_pay = DecimalField()
    season = StringField(choices=SEASON)
    number_nights = IntField()

class Booking(Document):
    id_place = StringField(required=True)
    listing_url = StringField(required=True)
    name = StringField()
    address = EmbeddedDocumentField(Address)
    property_type = StringField(choices=PROPERTY_TYPE)
    price=DecimalField(precision=2)
    security_deposit = DecimalField()
    customers = EmbeddedDocumentListField(Customer)

data1 = ListingsAndReviews.objects(beds=3)
pprint(data1)

for i in data1:
    print(i.name)
else:
    print("Final bucle")

# Això és per explicar a classe
data1 = ListingsAndReviews.objects()
data_1=data1[0]

booking1 = Booking(id_place= str(data_1.id),listing_url = data_1['listing_url'], name = data_1['name'], address=data_1['address'],
                   property_type = data_1['property_type'], price = data_1['price'] )
booking1.save()
if data_1['security_deposit'] != None:
    booking1['security_deposit'] = data_1['security_deposit']
    booking1.save()
booking1.delete()

